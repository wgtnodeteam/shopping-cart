var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');

var Order  = require('../models/order');
var Cart  = require('../models/cart');

var csrfProtection = csrf();
router.use(csrfProtection);

router.get('/profile', isLoggedIn,function(req, res, next){
	var succMsg = req.flash('success');
	var errMsg = req.flash('error');
	var msgType, hasMsg, messages = '';
	if(succMsg.length > 0){
		hasMsg = true;
		msgType = 'success';
		messages = succMsg;
	}
	if(errMsg.length > 0){
		hasMsg = true;
		msgType = 'danger';
		messages = errMsg;
	}
	res.render('user/profile', {csrfToken: req.csrfToken(), messages: messages, hasMsg: hasMsg, msgType: msgType});
});

router.post('/profile', isLoggedIn, function(req, res, next){
	if(req.files.file.name){
		req.files.file.mv('public/uploads/'+Date.now()+'_'+req.files.file.name, function(err){
			if(err){
				//return res.status(500).send(err);
				req.flash('error','Something happend wrong!');
				return res.redirect('/user/profile');
			}
			req.flash('success', 'Successfully update you profile!');
			return res.redirect('/user/profile');
		});
	}else{
		req.flash('error', 'Successfully update you profile without image!');
		return res.redirect('/user/profile');
	}
});

router.get('/order-history', isLoggedIn,function(req, res, next){
	Order.find({user: req.user}, function(err, orders){
		if(err){
			return res.write('Error!');
		}
		var cart;
		orders.forEach(function(order){
			cart = new Cart(order.cart);
			order.items = cart.generateArray();
		})
		res.render('user/order-history', {orders: orders});
	})
});

router.get('/logout', isLoggedIn, function(req, res, next){
	req.logout();
	res.redirect('/');
});

router.use('/', notLoggedIn, function(req, res, next){
	next();
});

router.get('/signup', function(req, res, next){
	var messages = req.flash('error');
	res.render('user/signup', {csrfToken: req.csrfToken(), messages: messages, hasError: messages.length > 0})
});

router.post('/signup', passport.authenticate('local.signup', {
	successRedirect: '/user/profile',
	failureRedirect: '/user/signup',
	failureFlash: true
}), function(req, res, next){
	if(req.session.oldUrl){
		var oldUrl = req.session.oldUrl;
		req.session.oldUrl = null;
		res.redirect(oldUrl);
	}else{
		res.redirect('/user/profile');
	}
});

router.get('/signin', function(req, res, next){
	var messages = req.flash('error');
	res.render('user/signin', {csrfToken: req.csrfToken(), messages: messages, hasError: messages.length > 0})
});

router.post('/signin', passport.authenticate('local.signin', {
	failureRedirect: '/user/signin',
	failureFlash: true
}), function(req, res, next){
	if(req.session.oldUrl){
		var oldUrl = req.session.oldUrl;
		req.session.oldUrl = null;
		res.redirect(oldUrl);
	}else{
		res.redirect('/user/profile');
	}
});

module.exports = router;

function isLoggedIn(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('/');
}

function notLoggedIn(req, res, next){
	if(!req.isAuthenticated()){
		return next();
	}
	res.redirect('/');
}